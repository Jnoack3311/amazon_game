#!/bin/bash

function cleanup() {
  docker-compose down
}

trap cleanup EXIT

docker-compose up -d

php -S localhost:8000 -t public/