$(document).ready(() => {
    $('#reveal_button').on('click', () => {
        $('#price').css('opacity', '1');
    });
    $('#new_round_button').on('click', () => {
        $('#new_round_button').addClass('loading');
        $('#loading-screen, #backdrop').css('display', 'block');
        window.location.href = window.location.href;
    });

    const es = new EventSource('http://localhost:3000/.well-known/mercure?topic=new_product');
    es.onmessage = e => {
        // Will be called every time an update is published by the server
        const response = JSON.parse(e.data);
        $('#price').text(response.article_price);
        $('#article_name').text(response.article_name);
        $('#product_image').css('background-image', 'url("' + response.product_image + '")');
    }
})

window.onload = function () {
    $('#loading-screen, #backdrop').css('display', 'none');
}