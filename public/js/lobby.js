const newUserEventSource = new EventSource('http://localhost:3000/.well-known/mercure?topic=' + encodeURIComponent('new_entered_user'));
const startGameEventSource = new EventSource('http://localhost:3000/.well-known/mercure?topic=' + encodeURIComponent('start_game'));

newUserEventSource.onmessage = event => {
    const eventData = JSON.parse(event.data);
    const players = JSON.parse(eventData.players);

    const playersHtml = [];

    $.each(players, function(key, player) {
        playersHtml.push('<p class="player">' + player.name +'</p>');
    });
    $('.players').html(playersHtml);
}

startGameEventSource.onmessage = event => {
    window.location.replace("/game");
}
