<?php

namespace App\Entity;

use App\Repository\MatchRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MatchRepository::class)
 * @ORM\Table(name="`match`")
 */
class Match
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var integer|null
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="match", cascade={"remove"})
     *
     * @var Collection
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Guess", mappedBy="match", cascade={"remove"})
     *
     * @var Collection
     */
    private $guesses;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lobby", inversedBy="matches")
     *
     * @var Lobby|null
     */
    private $lobby;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->guesses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function getGuesses(): Collection
    {
        return $this->guesses;
    }

    public function getLobby(): ?Lobby
    {
        return $this->lobby;
    }

    public function setLobby(?Lobby $lobby): void
    {
        $this->lobby = $lobby;
    }
}
