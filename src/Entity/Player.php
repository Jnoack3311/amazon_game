<?php

namespace App\Entity;

use App\Repository\PlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

/**
 * @ORM\Entity(repositoryClass=PlayerRepository::class)
 * @ORM\Table(name="`player`")
 */
class Player implements \JsonSerializable
{
    /**
     * @ORM\Id
     * @ORM\Column(type="ulid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @var Ulid|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=2, nullable=true)
     *
     * @var float|null
     */
    private $bid;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var integer|null
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lobby", inversedBy="players")
     *
     * @var Lobby|null
     */
    private $lobby;

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Guess", mappedBy="player")
     *
     * @var Collection
     */
    private $guesses;

    public function __construct()
    {
        $this->guesses = new ArrayCollection();
    }

    public function getId(): ?Ulid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBid(): ?string
    {
        return $this->bid;
    }

    public function setBid(?string $bid): self
    {
        $this->bid = $bid;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getLobby(): ?Lobby
    {
        return $this->lobby;
    }

    public function setLobby(?Lobby $lobby): void
    {
        $this->lobby = $lobby;
    }

    public function getGuesses(): Collection
    {
        return $this->guesses;
    }

    public function addGuess(Guess $guess): void
    {
        if ($this->guesses->contains($guess)) {
            return;
        }
        $this->guesses[] = $guess;
        $guess->setPlayer($this);
    }

    /**
     * @return string[][]
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id->toBase32(),
            'name' => $this->name,
            'score' => $this->score,
        ];
    }
}