<?php
declare(strict_types=1);

/*
 * Created by netlogix GmbH & Co. KG
 *
 * @copyright netlogix GmbH & Co. KG
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GuessRepository;

/**
 * @ORM\Entity(repositoryClass=GuessRepository::class)
 * @ORM\Table(name="`guess`")
 */
class Guess
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var integer|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string|null
     */
    private $guess;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Player", inversedBy="guesses")
     *
     * @var Player|null
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Match", inversedBy="guesses")
     *
     * @var Match|null
     */
    private $match;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGuess(): ?string
    {
        return $this->guess;
    }

    public function setGuess(?string $guess): void
    {
        $this->guess = $guess;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): void
    {
        $this->player = $player;
    }

    public function getMatch(): ?Match
    {
        return $this->match;
    }

    public function setMatch(?Match $match): void
    {
        $this->match = $match;
    }
}
