<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\Table(name="`product`")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var integer|null
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     *
     * @var string|null
     */
    private $Name;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string|null
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     *
     * @var string|null
     */
    private $link;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string|null
     */
    private $productAsin;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string|null
     */
    private $category;

    /**
     * @ORM\Column(type="text")
     *
     * @var string|null
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Match", inversedBy="products")
     *
     * @var Match|null
     */
    private $match;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getProductAsin(): ?string
    {
        return $this->productAsin;
    }

    public function setProductAsin(string $productAsin): self
    {
        $this->productAsin = $productAsin;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getMatch(): Match
    {
        return $this->match;
    }
}
