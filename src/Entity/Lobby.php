<?php
declare(strict_types=1);

/*
 * Created by netlogix GmbH & Co. KG
 *
 * @copyright netlogix GmbH & Co. KG
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\LobbyRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Ulid;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

/**
 * @ORM\Entity(repositoryClass=LobbyRepository::class)
 * @ORM\Table(name="`lobby`")
 * @UniqueEntity("key")
 */
class Lobby
{
    /**
     * @ORM\Id
     * @ORM\Column(type="ulid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UlidGenerator::class)
     *
     * @var Ulid|null
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Match", mappedBy="lobby", cascade={"remove"})
     *
     * @var Collection
     */
    private $matches;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Player", mappedBy="lobby", cascade={"remove"})
     *
     * @var Collection
     */
    private $players;

    public function __construct()
    {
        $this->matches = new ArrayCollection();
        $this->players = new ArrayCollection();
    }

    public function getId(): ?Ulid
    {
        return $this->id;
    }

    public function getMatches(): Collection
    {
        return $this->matches;
    }

    public function addMatch(Match $match): void
    {
        if ($this->matches->contains($match)) {
            return;
        }
        $this->matches[] = $match;
        $match->setLobby($this);
    }

    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): void
    {
        if ($this->players->contains($player)) {
            return;
        }
        $this->players[] = $player;
        $player->setLobby($this);
    }
}
