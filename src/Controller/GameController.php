<?php
declare(strict_types=1);

/*
 * Created by netlogix GmbH & Co. KG
 *
 * @copyright netlogix GmbH & Co. KG
 */

namespace App\Controller;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;

class GameController extends AbstractController
{
    public function index(PublisherInterface $publisher): Response
    {
        $success = false;
        while (!$success) {
            try {
                $data = $this->getProductData();
            } catch (\Throwable $exception) {
                continue;
            }

            if (empty($data['price'])) {
                continue;
            }

            $success = true;
        }

        $update = new Update(
            'new_product',
            json_encode([
                'product_category' => $data['category'],
                'article_asin' => $data['ASIN'],
                'article_name' => $data['product_name'],
                'article_price' => $data['price'] . '$',
                'product_image' => $data['product_image'],
                'amazon_link' => $data['amazon_link'],
            ])
        );

        $publisher($update);


        return $this->render('game.html.twig', [
            'product_category' => $data['category'],
            'article_asin' => $data['ASIN'],
            'article_name' => $data['product_name'],
            'article_price' => $data['price'] . '$',
            'product_image' => $data['product_image'],
            'amazon_link' => $data['amazon_link'],
            'showLobby' => false,
        ]);
    }

    private function getProductData(): array
    {
        $guzzle = new Client();
        $response = $guzzle->get('https://randomazonbackend.appspot.com/product/');
        $data = json_decode($response->getBody()->getContents(), true);
        $data['amazon_link'] = 'https://www.amazon.com/-/de/dp/' . $data['ASIN'];
        $response = $guzzle->get($data['amazon_link']);
        $responseHtml = $response->getBody()->getContents();
        $crawler = new Crawler($responseHtml);
        $data['product_name'] = $crawler->filter('#productTitle')->text();
        $data['product_image'] = "https://ws-na.amazon-adsystem.com/widgets/q?_encoding=UTF8&MarketPlace=US&ASIN=" . $data['ASIN'] . "&ServiceVersion=20070822&ID=AsinImage&WS=1&Format=UL1500";
        return $data;
    }
}
