<?php

namespace App\Controller;

use App\Entity\Lobby;
use App\Factory\LobbyFactory;
use App\Factory\PlayerFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

class LobbyController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var LobbyFactory */
    private $lobbyFactory;

    /** @var PlayerFactory */
    private $playerFactory;

    /** @var SessionInterface */
    private $session;

    /** @var SerializerInterface */
    private $serializer;

    public function __construct(
        EntityManagerInterface $entityManager,
        LobbyFactory $lobbyFactory,
        PlayerFactory $playerFactory,
        SessionInterface $session,
        SerializerInterface $serializer
    ) {
        $this->entityManager = $entityManager;
        $this->lobbyFactory = $lobbyFactory;
        $this->playerFactory = $playerFactory;
        $this->session = $session;
        $this->serializer = $serializer;
    }

    public function index(string $key): Response
    {
        try {
            $uuid = Uuid::fromString($key);
        } catch (\Throwable $exception) {
            return $this->render('lobby/lobby_not_exist.html.twig');
        }
        $lobbyRepository = $this->entityManager->getRepository(Lobby::class);
        $lobby = $lobbyRepository->find($uuid);

        if (null === $lobby) {
            return $this->render('lobby/lobby_not_exist.html.twig');
        }
        $players = $lobby->getPlayers();

        return $this->render('lobby/lobby.html.twig', [
            'lobbyKey' => $key,
            'players' => $players
        ]);
    }

    public function createLobby(Request $request): Response
    {
        $username = $request->get('username', '');
        if ('' === $username) {
            return $this->render('lobby/create_lobby.html.twig');
        }
        $lobby = $this->lobbyFactory->create();
        $player = $this->playerFactory->create($username);

        $lobby->addPlayer($player);

        $this->entityManager->persist($lobby);
        $this->entityManager->persist($player);

        $this->entityManager->flush();

        $this->session->set('user', $player->getId());
        $key = $lobby->getId()->toBase32();

        return $this->redirectToRoute('lobby', [
            'key' => $key,
        ]);
    }

    public function enterLobby(Request $request, PublisherInterface $publisher): Response
    {
        $key = $request->get('lobby_key', '');
        $username = $request->get('username', '');
        if ('' === $key || '' === $username) {
            return $this->render('lobby/enter_lobby.html.twig');
        }

        try {
            $uuid = Uuid::fromString($key);
        } catch (\Throwable $exception) {
            return $this->render('lobby/enter_lobby.html.twig');
        }
        $player = $this->playerFactory->create($username);

        $lobbyRepository = $this->entityManager->getRepository(Lobby::class);
        $lobby = $lobbyRepository->find($uuid);

        if (null === $lobby) {
            return $this->render('lobby/enter_lobby.html.twig');
        }
        $lobby->addPlayer($player);

        $this->entityManager->persist($player);
        $this->entityManager->flush();

        $this->session->set('user', $player->getId());
        $jsonPlayers = $this->serializer->serialize($lobby->getPlayers(), 'json');

        $update = new Update(
            'new_entered_user',
            json_encode([
                'user_id' => $player->getId(),
                'lobby_key' => $key,
                'players' => $jsonPlayers
            ])
        );
        $publisher($update);

        return $this->redirectToRoute('lobby', ['key' => $key]);
    }

    public function startGame(PublisherInterface $publisher): Response
    {
        $update = new Update('start_game');
        $publisher($update);

        return $this->redirectToRoute('game');
    }
}