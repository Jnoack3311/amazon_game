<?php
declare(strict_types=1);

/*
 * Created by netlogix GmbH & Co. KG
 *
 * @copyright netlogix GmbH & Co. KG
 */

namespace App\Repository;

use App\Entity\Guess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Guess|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guess|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guess[]    findAll()
 * @method Guess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Guess::class);
    }
}
