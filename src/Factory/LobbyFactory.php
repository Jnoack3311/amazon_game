<?php
declare(strict_types=1);

/*
 * Created by netlogix GmbH & Co. KG
 *
 * @copyright netlogix GmbH & Co. KG
 */

namespace App\Factory;

use App\Entity\Lobby;

class LobbyFactory
{
    public function create(): Lobby
    {
        $lobby = new Lobby();

        return $lobby;
    }
}
