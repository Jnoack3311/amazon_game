<?php
declare(strict_types=1);

/*
 * Created by netlogix GmbH & Co. KG
 *
 * @copyright netlogix GmbH & Co. KG
 */

namespace App\Factory;

use App\Entity\Player;

class PlayerFactory
{
    public function create(string $name): Player
    {
        $player = new Player();
        $player->setName($name);

        return $player;
    }
}
